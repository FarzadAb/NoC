#include <iostream>
#include <fstream>
#include <vector>
#include <cstdio>
#include <cstring>
using namespace std;

const int Dim = 2;
const int NumNodes = Dim*Dim*Dim;

#define PORT_X_P 1
#define PORT_X_N 2
#define PORT_Y_P 3
#define PORT_Y_N 4
#define PORT_Z_P 5
#define PORT_Z_N 6

int routerNum(int num) {
  int z = num%Dim, y = (num/Dim)%Dim, x = ((num/(Dim*Dim))%Dim);
  return z + (y << 4) + (x << 8);
}

void parse_router_file(ofstream* out, const char* router_fn)
{
  (*out) << hex;
  FILE *router_configuration_file = fopen(router_fn, "r");
  char str[100];
  int num_vcs = 8;
  int src_router, src_out_port, dest_router, dest_in_port;
  int num_credit_delay_cycles = 1;

  if (router_configuration_file == NULL) {
    printf("could not open %s\n", router_fn);
    return;
  }

  while (fgets(str, 100, router_configuration_file) != NULL) {
    if (sscanf(str, "num_credit_delay_cycles=%d", &num_credit_delay_cycles) == 1) {
      printf("setting num_credit_delay_cycles = %d\n", num_credit_delay_cycles);
    } else if (sscanf(str, "num_vcs=%d", &num_vcs) == 1) {
      // printf("setting num_vcs = %d\n", num_vcs);
    } else if (sscanf(str, "%d:%d-%d:%d", &src_router, &src_out_port, &dest_router, &dest_in_port) == 4) {
    }
  }
  fclose(router_configuration_file);
  (*out) << num_credit_delay_cycles << endl;
}

int traffic[NumNodes];
vector<int> dst[NumNodes];
vector<int> vcs[NumNodes];
vector<int> nfs[NumNodes];
int route_table[NumNodes][NumNodes];

void parse_traffic_file(ofstream* out, const char *traffic_fn) {
  (*out) << hex;
  FILE *traffic_file = fopen(traffic_fn, "r");
  char str[100];

  if (traffic_file == NULL) {
    printf("could not open %s\n", traffic_fn);
    return;
  }

  int max_cycle = 1000;
  while (fgets(str, 100, traffic_file) != NULL) {
    int src;
    int dest;
    int num_flits;
    int ret;
    int num_packets_to_send;
    int verbose;
    int vc;
    int out_port;

    if ((str[0] == '%') || (str[0] == '\n')) {
    } else if ((ret = sscanf(str, "verbose=%d", &verbose)) == 1) {
    } else if ((ret = sscanf(str, "max_cycle=%d", &max_cycle)) == 1) {
      printf("setting max_cycle = %d\n", max_cycle);
    } else if ((ret = sscanf(str, "route:%d->%d:%d", &src, &dest, &out_port)) == 3) {
      route_table[src][dest] = out_port;
    } else if ((ret = sscanf(str, "node %d:%d\n", &src, &num_packets_to_send)) == 2) {
      traffic[src] = num_packets_to_send;
    } else if ((ret = sscanf(str, "%d:%d:%d:%d\n", &src, &dest, &vc, &num_flits)) == 4) {
      dst[src].push_back(dest);
      vcs[src].push_back(vc);
      nfs[src].push_back(num_flits);
    } else if (!strncmp("end", str, 3)) {
      break;
    } else {
      printf("couldn't parse \"%s\"\n", str);
    }
  }
  (*out) << max_cycle << endl;

  for(int i=0; i<NumNodes; i++) {
    for(int j=0; j<NumNodes; j++)
      (*out) << route_table[i][j] << " ";
    (*out) << endl;
  }

  for(int i=0; i<NumNodes; i++) {
    for(int j=0; j<traffic[i]; j++)
      (*out) << routerNum(dst[i][j%((int)dst[i].size())]) << " "
             << vcs[i][j%((int)dst[i].size())] << " "
             << nfs[i][j%((int)dst[i].size())] << "  ";
    (*out) << (1<<12) << endl;
  }
}

void default_routing()
{
  for(int i=0; i<NumNodes; i++)
    for(int j=0; j<NumNodes; j++) {
      int z1 = i%Dim, y1 = (i/Dim)%Dim, x1 = (i/(Dim*Dim))%Dim;
      int z2 = j%Dim, y2 = (j/Dim)%Dim, x2 = (j/(Dim*Dim))%Dim;
      if( x1 != x2 )
        route_table[i][j] = PORT_X_P;
      else if( y1 != y2 )
        route_table[i][j] = PORT_Y_P;
      else if( z1 != z2 )
        route_table[i][j] = PORT_Z_P;
    }
}

int main()
{
  ofstream out("data.hex");
  string router_fn = "router.txt";
  string traffic_fn = "traffic.txt";
  default_routing();
  parse_router_file(&out, router_fn.c_str());
  parse_traffic_file(&out, traffic_fn.c_str());
  out.close();
  return 0;
}
