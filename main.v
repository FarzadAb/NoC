`ifndef __MAIN__
`define __MAIN__

`include "defaults.v"
`include "router.v"
`include "processorTraffic.v"
`include "clockGen.v"

module main (
	input clock, reset ,
	output done_cycles,
	output [`MAX_CYCLE_WIDTH-1:0] cycle_num);
  localparam NumClocks = `NUM_CLOCKS,
             FlitWidth = `DEFAULT_FLIT_WIDTH,
             N = `DIMENSION,
             PortNum = `PORT_NUM,
             AllNodes = N*N*N,
             PortSize = `PORT_SIZE,
			       TrafficTableEntryNums = `MAX_TRAFFIC_NUM*3;

  wire  [NumClocks-1:0] stage;
  wire  [FlitWidth-1:0] port_conn [0:N-1][0:N-1][0:N-1][1:PortNum-1];
  wire  [FlitWidth-1:0] proc_inj  [0:N-1][0:N-1][0:N-1];
  wire  [FlitWidth-1:0] proc_exp  [0:N-1][0:N-1][0:N-1];
  wire  [`DEFAULT_VC_NUM-1:0] credit [0:N-1][0:N-1][0:N-1][0:PortNum];//TODO
  wire [`NUM_CLOCKS-1:0] reset_signals;
  reg [`DELAY_WIDTH-1:0]  delay;
  reg [`MAX_CYCLE_WIDTH-1:0] max_cycles;

  wire[`ROUTE_TABLE_PER_NODE_SIZE-1:0] route_table[0:AllNodes-1];
  reg [PortSize-1:0] route_arr [0:AllNodes-1][0:AllNodes-1];
  wire [`MAX_TRAFFIC_SIZE*TrafficTableEntryNums-1:0] traffic_table[0:AllNodes-1];
  reg [`MAX_TRAFFIC_SIZE-1:0] traffic_arr[0:AllNodes-1][0:TrafficTableEntryNums-1];
  wire done_router_p[0:N-1][0:N-1][0:N-1];
  wire done_processor_p[0:N-1][0:N-1][0:N-1];

  wire [N*N*N-1:0] done_object;
  wire done_all;

  assign done_all = &done_object;

  genvar i, j , k ,  x , y , z;
  generate
	for (i = 0 ; i < N ; i = i+1) begin
		for (j = 0 ; j < N ; j = j+1) begin
			for (k = 0 ; k < N ; k = k+1) begin
				assign done_object[i*N*N+j*N+k] = done_router_p[i][j][k]&done_processor_p[i][j][k];
			end
		end
	end

	for (i = 0 ; i < AllNodes ; i = i+1) begin
		for (j = 0 ; j < TrafficTableEntryNums ; j = j+1) begin
			assign traffic_table[i][((j+1)*`MAX_TRAFFIC_SIZE)-1:j*`MAX_TRAFFIC_SIZE] =
					traffic_arr[i][j];
		end
	end

	for (i = 0 ; i < AllNodes ; i = i+1) begin
		// for each route_table_entry
		for (j = 0 ; j < AllNodes ; j = j+1) begin
			assign route_table[i][((j+1)*PortSize-1):j*PortSize] = route_arr[i][j];
		end
	end

	for(x=0; x<N; x=x+1) begin : X
      for(y=0; y<N; y=y+1) begin : Y
        for(z=0; z<N; z=z+1) begin : Z
			processorTraffic pt(
				.clock({stage[`Proc_Phase], stage[`Proc_Reset]})
				, .reset(reset_signals`RESET_PROC)
				, .inpTrafficTable(traffic_table[x*N*N+y*N+z])
				, .has_data(credit[x][y][z][0])
				, .flit_out(proc_inj[x][y][z])
				, .done_processor_p(done_processor_p[x][y][z]));

          router r (
            .done_router_p(done_router_p[x][y][z]),
            .out_ports({
              port_conn[x][y][z][`PORT_Z_Neg],
              port_conn[x][y][z][`PORT_Z_Pos],
              port_conn[x][y][z][`PORT_Y_Neg],
              port_conn[x][y][z][`PORT_Y_Pos],
              port_conn[x][y][z][`PORT_X_Neg],
              port_conn[x][y][z][`PORT_X_Pos],
              proc_exp[x][y][z]
            }),
            .credit_by_in_ports({
							credit[x][y][(z==0?N-1:z-1)][`PORT_Z_Pos],
              credit[x][y][(z==N-1?0:z+1)][`PORT_Z_Neg],
              credit[x][(y==0?N-1:y-1)][z][`PORT_Y_Pos],
              credit[x][(y==N-1?0:y+1)][z][`PORT_Y_Neg],
              credit[(x==0?N-1:x-1)][y][z][`PORT_X_Pos],
              credit[(x==N-1?0:x+1)][y][z][`PORT_X_Neg],
							credit[x][y][z][`PORT_Proc]
            }),
            .in_ports({
              port_conn[x][y][(z==0?N-1:z-1)][`PORT_Z_Pos],
              port_conn[x][y][(z==N-1?0:z+1)][`PORT_Z_Neg],
              port_conn[x][(y==0?N-1:y-1)][z][`PORT_Y_Pos],
              port_conn[x][(y==N-1?0:y+1)][z][`PORT_Y_Neg],
              port_conn[(x==0?N-1:x-1)][y][z][`PORT_X_Pos],
              port_conn[(x==N-1?0:x+1)][y][z][`PORT_X_Neg],
              proc_inj[x][y][z]
            }),
            .credit_from_out_ports({
							credit[x][y][z][`PORT_Z_Neg],
              credit[x][y][z][`PORT_Z_Pos],
              credit[x][y][z][`PORT_Y_Neg],
              credit[x][y][z][`PORT_Y_Pos],
              credit[x][y][z][`PORT_X_Neg],
              credit[x][y][z][`PORT_X_Pos],
              credit[x][y][z][PortNum]
            }),
			      .routing_table(route_table[x*N*N+y*N+z]),
						.credit_delay(delay),
      			.clock(stage),
						.reset(reset_signals`RESET_PROC)
          );
		end
	  end
	end
  endgenerate

  clockGen cg(
	.stage(stage),
	.reset_signals(reset_signals),
	.clock(clock),
	.reset(reset),
	.done_all(done_all),
	.cycle_num(cycle_num),
	.max_cycles(max_cycles),
	.done_cycles(done_cycles));


  task init; begin
    readFile;
  end
  endtask

	integer ii, jj;
	// integer kk;
	// always @(proc_exp) begin
	// 	for(ii=0; ii<N; ii=ii+1)begin
	// 		for(jj=0; jj<N; jj=jj+1)begin
	// 		 for(kk=0; kk<N; kk=kk+1)begin
	// 			if( proc_exp[ii][jj][kk]`REAL_BIT == 1'b1 )
	// 				$display("proc %d%d%d port has data %b", ii, jj, kk, proc_exp[ii][jj][kk]);
	// 			end
	// 		end
	// 	end
	// end

  always @(posedge reset_signals`RESET_MAIN) begin
    init;
	end

	reg [`FILE_REC_WIDTH-1:0] mem [0: 3 + AllNodes*AllNodes + AllNodes * (3*`MAX_TRAFFIC_NUM+1)];
	integer startTraffic;

	task readFile;
	  begin
		$readmemh("data.hex", mem);

		delay = mem[0][4:0];
		max_cycles = mem[1];

		for(ii=0; ii<AllNodes; ii=ii+1)
		  for(jj=0; jj<AllNodes; jj=jj+1)
			route_arr[ii][jj] = mem[ii*AllNodes + jj + 2][PortSize-1:0];


		jj = AllNodes*AllNodes + 2;
		for(ii=0; ii<AllNodes; ii=ii+1) begin
			startTraffic = jj;
		  while( mem[jj] != 13'h1000 ) begin
			traffic_arr[ii][(jj-startTraffic)] 	= 	mem[jj][`MAX_TRAFFIC_SIZE-1:0];
			traffic_arr[ii][(jj-startTraffic+1)] 	= 	mem[jj+1][`MAX_TRAFFIC_SIZE-1:0];
			traffic_arr[ii][(jj-startTraffic+2)] 	= 	mem[jj+2][`MAX_TRAFFIC_SIZE-1:0];
			jj = jj+3;
		  end
		  traffic_arr[ii][(jj-startTraffic)] = 0;
		  traffic_arr[ii][(jj-startTraffic)+1] = 0;
		  traffic_arr[ii][(jj-startTraffic)+2] = 0;

		  jj = jj+1;
		end

	  end
	endtask

endmodule

`endif
