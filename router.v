`include "inPort.v"
`include "inPortProc.v"
`include "controller.v"
`include "defaults.v"

module router
  (
    output done_router_p, // TODO
    output [`PORT_NUM*`FLIT_WIDTH-1	:0] out_ports,
    output [`PORT_NUM*`DEFAULT_VC_NUM-1		:0] credit_by_in_ports,
    input  [`PORT_NUM*`FLIT_WIDTH-1	:0] in_ports,
    input  [`PORT_NUM*`DEFAULT_VC_NUM-1		:0] credit_from_out_ports,
    input  [`ROUTE_TABLE_PER_NODE_SIZE-1	:0] routing_table,
    input  [`DELAY_WIDTH-1:0] credit_delay,
    input [`NUM_CLOCKS-1:0] clock,
	input reset // TODO : we should have some number of clocks not only one , what to do?
  );

  localparam FLIT_ARRAY_WIDTH = `FLIT_WIDTH*`DEFAULT_VC_NUM;
  wire[FLIT_ARRAY_WIDTH*`PORT_NUM-1:0] flit_out_inPort;
  wire[`DEFAULT_VC_NUM*`PORT_NUM-1:0] has_data, clear;
  wire[`DEFAULT_VC_NUM-1:0] tmp;

	// Port 0 has no delays. so we use has_data instead of actual credits.
	assign credit_by_in_ports[`DEFAULT_VC_NUM*(0+1)-1:`DEFAULT_VC_NUM*0] = has_data[`DEFAULT_VC_NUM*(0+1)-1:`DEFAULT_VC_NUM*0];

	  inPortProc routerInPortProc(
        .clock(clock[`Phase0]/*???which clock*/),
		.reset(reset),
        .flit_in(in_ports[(0+1)*`FLIT_WIDTH-1:0*`FLIT_WIDTH]),
        .clear(clear[`DEFAULT_VC_NUM*(0+1)-1:`DEFAULT_VC_NUM*0]),
        .credit(tmp),
        .has_data(has_data[`DEFAULT_VC_NUM*(0+1)-1:`DEFAULT_VC_NUM*0]),
        .flit_out(flit_out_inPort[FLIT_ARRAY_WIDTH*(0+1)-1: FLIT_ARRAY_WIDTH*0]),
		.credit_delay(credit_delay)
        );

  genvar i;
  generate
    for(i=1; i<`PORT_NUM; i=i+1)begin
      inPort routerInPort(
        .clock(clock[`Phase0]/*???which clock*/), .reset(reset),
        .flit_in(in_ports[(i+1)*`FLIT_WIDTH-1:i*`FLIT_WIDTH]),
        .clear(clear[`DEFAULT_VC_NUM*(i+1)-1:`DEFAULT_VC_NUM*i]),
        .credit(credit_by_in_ports[`DEFAULT_VC_NUM*(i+1)-1:`DEFAULT_VC_NUM*i]),
        .has_data(has_data[`DEFAULT_VC_NUM*(i+1)-1:`DEFAULT_VC_NUM*i]),
        .flit_out(flit_out_inPort[FLIT_ARRAY_WIDTH*(i+1)-1: FLIT_ARRAY_WIDTH*i]),
		    .credit_delay(credit_delay)
        );

    end
  endgenerate

	controller routerController(
    .reset(reset),
		.clock({clock[`Phase1],clock[`Ctrl_Reset]}),
		.has_data_vector(has_data),
		.flit_in_vector(flit_out_inPort),
		.routing_table_vector(routing_table),
		.credits(credit_from_out_ports),
		.clear_vector(clear),
		.flit_out_vector(out_ports),
		.done_router_p(done_router_p)
	);

endmodule
