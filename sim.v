`include "defaults.v"

module sim;
	reg clock, reset;
	wire done_cycles;
	wire [`MAX_CYCLE_WIDTH-1:0] cycle_num;
	
	main m (clock, reset, done_cycles,cycle_num);
	
	always #4 clock = ~clock;
	
	initial begin
		reset = 0;
		clock = 0;
		#1 reset = 1;
		#1 reset = 0;
	end
endmodule