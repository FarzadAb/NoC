`ifndef __VC_BUFFER_IN__
`define __VC_BUFFER_IN__

`include "defaults.v"


module vc_buffer_in
         (output reg [`DEFAULT_FLIT_WIDTH-1:0] data_out, output reg credit, has_data,
          input [`DEFAULT_FLIT_WIDTH-1:0] data_in,
		  input [`DELAY_WIDTH-1:0] credit_delay,
		  input load, clear, clock, reset);

    localparam wait_inp_state = 2'b00, wait_out_state = 2'b01, delay_state = 2'b10;
    reg[1:0] state;

    reg[`DELAY_WIDTH-1:0] cnt;

    task init; begin
      state = wait_inp_state;
      cnt = 0;
      data_out = 0;
      has_data = 0;
      credit = 1;
    end
    endtask

    initial
        init;

    always @(posedge clock or posedge clear or posedge reset)
    begin
        if( reset )
          init;
        else begin
          // $display($time, " ++++++++ in vc_buffer_in %m load is %b, state is %b, data_in = %b", load, state, data_in);
          case(state)
              wait_out_state: if( clear ) begin
                      // $display(" -------- in vc_buffer_in %m clear is %b, data_out = %b", clear, data_out);
                      // $display("******** %m entezaring!");
                      state = delay_state;
                      has_data = 0;
                      cnt = 0;
                      credit = 0;     // no change
                      data_out = 0;   // not really needed
                  end
                  else begin
                      state = wait_out_state;  // no change
                      has_data = 1;      // no change
                      cnt = 0;           // not really needed
                      credit = 0;        // no change
                      data_out = data_out;
                  end
              wait_inp_state: if( load ) begin
                      // $display(" ++++++++ in vc_buffer_in %m load is %b, data_in = %b", load, data_in);
                      state = wait_out_state;
                      data_out = data_in;
                      has_data = 1;
                      cnt = 0;        // no change
                      credit = 0;
                  end
                  else begin
                      state = wait_inp_state;  // no change
                      data_out = 0;      // no change
                      has_data = 0;      // no change
                      cnt = 0;           // not really needed
                      credit = 1;        // no change
                  end
              delay_state: if( cnt == credit_delay - 1 ) begin
                      // $display("$*$*$*$*$* %m entezar ha be sar resid!");
                      state = wait_inp_state;
                      credit = 1;
                      cnt = 0;
                      has_data = 0;   // no change
                      data_out = 0;   // no change
                  end
                  else begin
                      state = delay_state;
                      cnt = cnt+1;
                      credit = 0;     // no change
                      has_data = 0;   // no change
                      data_out = 0;   // no change
                  end
              default: begin          // safe state
                  state = wait_inp_state;
                  cnt = 0;
                  data_out = 0;
                  has_data = 0;
                  credit = 1;
              end
          endcase
        end
    end

endmodule

`endif
