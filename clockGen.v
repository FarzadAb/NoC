`ifndef __clock_gen__
`define __clock_gen__

`include "defaults.v"
`include "vc_buffer_in.v"

module clockGen
  (
    output reg [`NUM_CLOCKS-1:0] stage, reset_signals,
    output reg [`MAX_CYCLE_WIDTH-1:0] cycle_num,
    output reg done_cycles,
	 input [`MAX_CYCLE_WIDTH-1:0] max_cycles,
    input clock, reset, done_all
  );
  // max-clocks = 6
  reg [2:0] state;

  task init;
    begin

      state = 0;
      cycle_num = 0;
      reset_signals = 0;
      stage = 0;
      done_cycles = 0;
    end
  endtask

  task increment_state;
    begin
      if( state >= `NUM_CLOCKS-1 ) begin
        state = 0;
        $display("*****\\:D//*****  end of cycle %d ----------------------", cycle_num);
        cycle_num = cycle_num+1;
      end else begin
        state = state+1;
        cycle_num = cycle_num; // no change
      end
    end
  endtask

  initial
    init;

  always @(posedge clock or posedge reset or posedge done_all) begin
    /*    reset    */
    if( reset ) begin
      init;
    end

    /*     done    */
    else if( done_all || cycle_num >= max_cycles ) begin
      $display("done in cycle = %d", cycle_num);
      $stop;
      stage = 0;
      reset_signals = 0;
      state = 0;
      cycle_num = cycle_num;
      done_cycles = 1;
    end

    /*  cycle_zero  */
    else if( cycle_num == 0 ) begin
      done_cycles = 0;  // no change
      stage = 0;
      reset_signals = 0;
      reset_signals[state] = 1;

      increment_state;
    end

    /* normal cycles */
    else begin
      done_cycles = 0;  // no change
      reset_signals = 0;
      stage = 0;
      stage[state] = 1;

      increment_state;
    end

  end
endmodule

`endif
