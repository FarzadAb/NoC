`ifndef __DEFAULT_VALUES__
`define __DEFAULT_VALUES__

`define DIMENSION 2
`define N (`DIMENSION)
`define DIM_WIDTH 4  // don't change this!
`define MESH_SIZE (`DIMENSION*`DIMENSION*`DIMENSION)
`define DEFAULT_VC_NUM 8
`define DEFAULT_VC_NUM_WIDTH 3
`define DEFAULT_DELAY 1
`define DELAY_WIDTH 5
`define DEST_WIDTH (3*`DIM_WIDTH)
`define DEFAULT_FLIT_WIDTH (`DEST_WIDTH+6)
`define FLIT_WIDTH (`DEFAULT_FLIT_WIDTH)  // just for redundancy :)
`define MAX_CYCLE_WIDTH 17
`define FILE_REC_WIDTH (`MAX_CYCLE_WIDTH)

// clocks
`define NUM_CLOCKS 5  // currently this can't be more than 8
`define Proc_Reset 0
`define Proc_Phase 1
`define Phase0     2
`define Ctrl_Reset 3
`define Phase1     4

// reset
`define RESET_MAIN [0]
`define RESET_PROC [1]

// flit bits
`define HEAD_BIT [17]
`define TAIL_BIT [16]
`define DEST_BITS [15:4]
`define VC_BITS [3:1]
`define REAL_BIT [0]
`define NON_REAL_BITS [`DEFAULT_FLIT_WIDTH-1:1]

// port assignment
`define PORT_SIZE 3
`define PORT_NUM  7
`define PORT_Proc 0
`define PORT_X_Pos  1
`define PORT_X_Neg  2
`define PORT_Y_Pos  3
`define PORT_Y_Neg  4
`define PORT_Z_Pos  5
`define PORT_Z_Neg  6
`define DEST_X_BITS [11:8]
`define DEST_Y_BITS [7:4]
`define DEST_Z_BITS [3:0]
`define BITS_TO_DEST(bits) ((bits`DEST_X_BITS*`N*`N) + (bits`DEST_Y_BITS*`N) + (bits`DEST_Z_BITS))

// route table
`define ROUTE_TABLE_PER_NODE_SIZE (`MESH_SIZE*`PORT_SIZE)

// traffic
`define MAX_TRAFFIC_NUM 3
`define TRAFFIC_ENTRY_NUMS (`MAX_TRAFFIC_NUM*3)
`define MAX_TRAFFIC_SIZE (`DEST_WIDTH)

`endif
