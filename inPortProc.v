`ifndef __in_port_proc__
`define __in_port_proc__

`include "defaults.v"
`include "vc_buffer_proc_in.v"

module inPortProc
  (flit_out,
  credit, has_data,
  flit_in,
  clear,
  credit_delay,
  clock, reset
  );

  localparam FLIT_ARRAY_WIDTH = `DEFAULT_FLIT_WIDTH*`DEFAULT_VC_NUM;
    output[FLIT_ARRAY_WIDTH-1:0] flit_out;
	output [`DEFAULT_VC_NUM-1:0] credit, has_data;
    input[`DEFAULT_FLIT_WIDTH-1:0] flit_in;
	input[`DEFAULT_VC_NUM-1:0] clear;
	input[`DELAY_WIDTH-1:0] credit_delay;
	input clock, reset;

  wire [FLIT_ARRAY_WIDTH-1:0] data_in;
  reg [`DEFAULT_FLIT_WIDTH-1:0] data_in_arr [0:`DEFAULT_VC_NUM-1];

  reg [`DEFAULT_VC_NUM-1:0] load;
  reg [`DEFAULT_VC_NUM_WIDTH-1:0] last;

  genvar i;
  generate
    for(i=0; i< `DEFAULT_VC_NUM; i=i+1)begin
      assign data_in[(i+1)*`DEFAULT_FLIT_WIDTH-1:`DEFAULT_FLIT_WIDTH*i] = data_in_arr[i];
      vc_buffer_proc_in t(
        .data_out(flit_out[(i+1)*`DEFAULT_FLIT_WIDTH-1:`DEFAULT_FLIT_WIDTH*i]),
        .credit(credit[i]),
		.has_data(has_data[i]),
        .data_in(data_in[(i+1)*`DEFAULT_FLIT_WIDTH-1:`DEFAULT_FLIT_WIDTH*i]),
        .load(load[i]),
		.credit_delay(credit_delay),
		.clear(clear[i]),
		.clock(clock),
		.reset(reset) );
    end
  endgenerate

  integer j;
  task init;
  begin
    for(j=0; j<`DEFAULT_VC_NUM; j=j+1)begin
      data_in_arr[j] = 0;
    end
    load = 0;
    last = 0;
  end
  endtask


  initial
    init;

  always @ ( posedge flit_in`REAL_BIT or negedge flit_in`REAL_BIT or posedge reset ) begin
    if( reset ) begin
      init;
	  end
    else begin
      load = 0;
      if( flit_in`REAL_BIT == 1'b1 ) begin
      // check if we need to implement parity-bit
        last = flit_in`VC_BITS;
        load[last] = 1;
        data_in_arr[last] = flit_in;
        // #1 $display("inport +++ %m flit_in = %b, set for vc = %d", flit_in, last);
      end
      else begin
        data_in_arr[last] = 0;
        load[last] = 0;
        // #1 $display("inport --- %m flit_in = %b, set for vc = %d", flit_in, last);
      end
    end
  end

endmodule

`endif
