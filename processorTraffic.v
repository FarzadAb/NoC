// `include "F:/University/DSD/Project/code/defaults.v"
`include "defaults.v"

module processorTraffic
	(input [1:0] clock
	, input reset
	, input [`MAX_TRAFFIC_SIZE*`TRAFFIC_ENTRY_NUMS-1:0] inpTrafficTable/* Traffic table */
	, input [`DEFAULT_VC_NUM-1:0]has_data
	, output reg [`DEFAULT_FLIT_WIDTH-1:0]flit_out
	, output done_processor_p);

	localparam routerNumberStart = 0;
	localparam virtualChannelStart = 1;
	localparam lengthStart = 2;

	wire [`MAX_TRAFFIC_SIZE-1:0]traffic[0:`TRAFFIC_ENTRY_NUMS-1];
	reg [31:0] headQueue;
	reg [31:0] num;
	reg headBit, tailBit;

	reg finished;

	assign done_processor_p = finished;

	genvar i;
	generate
		for (i = 0 ; i < `TRAFFIC_ENTRY_NUMS ; i = i+1) begin
			assign traffic[i] = inpTrafficTable[((i+1)*`MAX_TRAFFIC_SIZE)-1:i*`MAX_TRAFFIC_SIZE];
		end
	endgenerate

	always @(posedge clock[0] or posedge clock[1] or posedge reset) begin
		if (reset == 1'b1) begin
			init;
		end
		else if (clock[`Proc_Reset] == 1'b1) begin
			// if( flit_out`REAL_BIT == 1'b1 )
			// 	$display("last flit on line was: --%m-- to dest %h through vc %d flit %b", traffic[headQueue+routerNumberStart][11:0], traffic[headQueue+virtualChannelStart][2:0], flit_out);
			flit_out = 0;
		end
		else begin
			if (finished == 1'b0 && has_data[traffic[headQueue+virtualChannelStart][2:0]] == 1'b0) begin // if (!finished)
				if (num+1 == traffic[headQueue+lengthStart])
					tailBit = 1'b1;
				else
					tailBit = 1'b0;

				if( num == 0 )
					headBit = 1'b1;
				else
					headBit = 1'b0;
				flit_out = 0;
						//  head      tail  			destination                    VirtualChannelBits  				Real
				// $display("$+++++$ setting flit_out for %m");
				flit_out = {headBit,tailBit,traffic[headQueue+routerNumberStart][11:0],traffic[headQueue+virtualChannelStart][2:0],1'b1};
				#1 $display("sending traffic from --%m-- to dest %h through vc %d, num %d, headBit %b flit %b", traffic[headQueue+routerNumberStart][11:0], traffic[headQueue+virtualChannelStart][2:0], num, headBit, flit_out);

				num = num+1;
				if (tailBit == 1'b1) begin // if(tail)
					headQueue = headQueue+3;
					num = 0;
					// headBit = 1'b1;
					if (headQueue >= `TRAFFIC_ENTRY_NUMS) begin
						finished = 1'b1;
						// $display("finishing %m");
					end else if (traffic[headQueue+lengthStart] == 0) begin
						finished = 1'b1;
						// $display("finishing %m");
					end

				end
				// else
				// 	$display("there are some more..................");
			end
		end
	end

	task init;
		begin
			headQueue = 0;
			num = 0;
			finished = 1'b0;
			if (traffic[headQueue+lengthStart] == 0) begin
				finished = 1'b1;
			end
		end
	endtask
endmodule
