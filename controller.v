`include "defaults.v"

module controller(clock,reset, has_data_vector,flit_in_vector,routing_table_vector,credits,clear_vector,flit_out_vector,done_router_p);
	localparam VC_NUM = `DEFAULT_VC_NUM;
	localparam NUMBER_OF_VC = VC_NUM*`PORT_NUM;
	localparam FLIT_IN_ARRAY_WIDTH = NUMBER_OF_VC*`FLIT_WIDTH;
	localparam FLIT_OUT_ARRAY_WIDTH = `PORT_NUM*`FLIT_WIDTH;

	input [1:0] clock;
	input reset;
	input [NUMBER_OF_VC-1				:0] has_data_vector ; // which vc's have data
	input [FLIT_IN_ARRAY_WIDTH-1		:0] flit_in_vector; // flit's in vc's
	input [`ROUTE_TABLE_PER_NODE_SIZE-1	:0] routing_table_vector; // routing table
	input [NUMBER_OF_VC-1				:0] credits; // credits from out_ports
	output[NUMBER_OF_VC-1				:0] clear_vector; // to be cleared
	output[FLIT_OUT_ARRAY_WIDTH-1		:0] flit_out_vector; // flit's going out from each port
	output done_router_p; // all router's are done or not? if there's remained data to be sent they are not done




	wire [`FLIT_WIDTH-1:0] flit_in[NUMBER_OF_VC-1:0];
	wire [`PORT_SIZE-1:0] routing_table[`MESH_SIZE-1:0];
	reg [`FLIT_WIDTH-1:0] flit_out[`PORT_NUM-1:0];
	reg [NUMBER_OF_VC-1:0] clear;

	assign clear_vector = clear;

	genvar i;
	generate
		for (i = 0 ; i < NUMBER_OF_VC ; i = i+1) begin
			assign flit_in[i] = flit_in_vector[(i+1)*`FLIT_WIDTH-1:i*`FLIT_WIDTH];
		end
		for (i = 0 ; i < `PORT_NUM ; i = i+1) begin
			assign flit_out_vector[(i+1)*`FLIT_WIDTH-1:i*`FLIT_WIDTH] = flit_out[i];
		end
		for (i = 0 ; i < `MESH_SIZE ; i = i+1) begin
			assign routing_table[i] = routing_table_vector[(i+1)*`PORT_SIZE-1:i*`PORT_SIZE];
		end
	endgenerate



	// regs
	reg	[`PORT_NUM-1:0] in_free_p;
	reg [`PORT_NUM-1:0] out_free_p;

	reg a[7:0];
	reg resrved[`PORT_NUM-1:0][VC_NUM-1:0];
	reg [`PORT_SIZE-1:0] reserved_port_number[`PORT_NUM-1:0][VC_NUM-1:0];

	reg [`DEST_WIDTH-1:0] virtualDest;
	reg [`DEST_WIDTH-1:0] dest;
	reg [`PORT_SIZE-1:0]out_port;
	reg done_router;
	assign done_router_p = done_router;

	// for integers
	integer vc;
	integer in_port;


	integer ii, jj;
	always @(posedge clock[0] or posedge clock[1] or posedge reset) begin
		if( reset ) begin
			for(ii=0; ii<`PORT_NUM; ii=ii+1) begin
				for(jj=0; jj<VC_NUM; jj=jj+1) begin
					resrved[ii][jj] = 0;
					reserved_port_number[ii][jj] = 0;
				end
			end
		end

		else if( clock[0] ) begin
			clear = 0;
			for(ii=0; ii<`PORT_NUM; ii=ii+1) begin
				flit_out[ii] = 0;
			end
		end

		else begin
			done_router = 1;
			in_free_p = {`PORT_NUM{1'b1}};
			out_free_p = {`PORT_NUM{1'b1}};

			for (vc = 0 ; vc < VC_NUM ; vc = vc+1) begin
				for (in_port = 0 ; in_port < `PORT_NUM ; in_port = in_port+1) begin
					if (has_data_vector[(in_port*VC_NUM)+vc] == 1'b1) begin
					end
					// if this in_port is not occupied by higher priority VC's (lower VC numbers)
					if (in_free_p[in_port] == 1'b1) begin
						//
						virtualDest = flit_in[(in_port*VC_NUM)+vc][15:4];//`DEST_BITS; // TODO az koja ta koja
						dest = `BITS_TO_DEST(virtualDest); //(virtualDest[11:8])*N*N+(virtualDest[7:4])*N+(virtualDest[3:0]);
						out_port = routing_table[dest];
						if (has_data_vector[(in_port*VC_NUM)+vc] == 1'b1) begin
							done_router = 0;
							// $display($time, " out_free_p[out_port] = %b, resrved[out_port][vc] = %b, reserved_port_number[out_port][vc] = %b", out_free_p[out_port], resrved[out_port][vc], reserved_port_number[out_port][vc]);
							// if( out_free_p[out_port] == 1'b0 || (resrved[out_port][vc] == 1'b1 && reserved_port_number[out_port][vc] != in_port) )

							if (out_free_p[out_port] == 1'b1 && (resrved[out_port][vc] == 1'b0 || reserved_port_number[out_port][vc] == in_port)) begin
								if (out_port == 0 || credits[(out_port*VC_NUM)+vc] == 1'b1) begin
									$display($time, " in ---%m--- sending flit %b, to dest %d => out_port is %b", flit_in[(in_port*VC_NUM)+vc], dest, out_port);
									//1 tail or else
									if (flit_in[(in_port*VC_NUM)+vc][16]/*`TAIL_BIT*/) begin// TODO : tail
										resrved[out_port][vc] = 1'b0;
										// if( flit_in[(in_port*VC_NUM)+vc][17] == 1'b0 )
										// 	$display("yu huuu  in --%m--- releasing out_port %d in vc %d", out_port, vc);
									end else begin
										// $display("&&&&  in --%m--- reserving out_port %d in vc %d", out_port, vc);
										resrved[out_port][vc] = 1'b1;
										reserved_port_number[out_port][vc] = in_port;
									end
									//2 in_free_p , out_free_p
									in_free_p[in_port] = 1'b0;
									out_free_p[out_port] = 1'b0;
									//3 clear
									clear[(in_port*VC_NUM)+vc] = 1'b1;
									//4 flit_out
									flit_out[out_port] = flit_in[(in_port*VC_NUM)+vc];
								end
								// else
								// 	$display("((((())))) --%m-- in_port = %d, credits_out[out_port=%d, vc=%d] = %d", in_port, out_port, vc, credits[(out_port*VC_NUM)+vc]);
							end
							// else
							// 	#1 $display(" ^^^^^^  blocked in %m, out_port %b, vc %d is reserved, reserved_port_number=%d but we are in_port=%d", out_port, vc, reserved_port_number[out_port][vc], in_port);
						end
					end
				end
			end
		end
	end

endmodule
